from payment_paypal import PaymentPayPal
from product import Product
from order import Order


order = Order()

product1 = Product('Notebook', 12000)
product2 = Product('Cellphone', 7000)
product3 = Product('Mouse', 400)

order.add_item(product1, 1)
order.add_item(product2, 1)
order.add_item(product3, 4)

total = order.total_price()
print(f'Total Price: {total}')

email_address = 12345

payment_process = PaymentPayPal(email_address)
payment_process.pay(order)