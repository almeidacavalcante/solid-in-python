from order import Order
from payment import PaymentProcess


class PaymentCredit(PaymentProcess):
    def pay(self, order: Order):
        print('paid with credit')
        order.close_order()