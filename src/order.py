from product import Product

class Order:
    items: Product = []
    quantities = []
    status = 'open'

    def close_order(self):
        self.status = 'closed'

    def add_item(self, product: Product, quantity):
        self.items.append(product)
        self.quantities.append(quantity)

    def total_price(self):
        total = 0
        for i in range(len(self.quantities)):
            total += self.quantities[i] * self.items[i].price
        return total

    # the pay method was removed from here to obay the single responsibility principle
