from abc import ABC, abstractmethod
from typing import AbstractSet
from order import Order


class PaymentProcess(ABC):
    @abstractmethod
    def pay(self, order):
        pass

