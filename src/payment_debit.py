from order import Order
from payment import PaymentProcess


class PaymentDebit(PaymentProcess):

    def __init__(self, security_code):
        self.security_code = security_code

    def pay(self, order: Order):
        print('paid with debit')
        print(f'verification code: {self.security_code}')
        order.close_order()