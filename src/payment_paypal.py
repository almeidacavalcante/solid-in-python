from payment import PaymentProcess
from order import Order


class PaymentPayPal(PaymentProcess):
    
    def __init__(self, email_address):
        self.email_address = email_address

    def pay(self, order: Order):
        print('paid with paypal')
        print(f'verification email: {self.email_address}')
        order.close_order()
